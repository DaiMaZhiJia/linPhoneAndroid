/*
 * Copyright (c) 2010-2019 Belledonne Communications SARL.
 *
 * This file is part of linphone-android
 * (see https://www.linphone.org).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.linphone.call;

import android.content.ContentResolver;
import android.content.Context;
import android.provider.Settings;
import android.widget.Toast;
import org.linphone.LinphoneContext;
import org.linphone.LinphoneManager;
import org.linphone.R;
import org.linphone.contacts.ContactsManager;
import org.linphone.contacts.LinphoneContact;
import org.linphone.core.Address;
import org.linphone.core.Call;
import org.linphone.core.CallParams;
import org.linphone.core.Core;
import org.linphone.core.MediaEncryption;
import org.linphone.core.ProxyConfig;
import org.linphone.core.tools.Log;
import org.linphone.dialer.views.AddressType;
import org.linphone.mediastream.Version;
import org.linphone.settings.LinphonePreferences;
import org.linphone.utils.FileUtils;
import org.linphone.utils.LinphoneUtils;

/** Handle call updating, reinvites. 处理呼叫更新，重新邀请。 */
public class CallManager {
    // 上下文
    private Context mContext;
    // 通话界面接口
    private CallActivityInterface mCallInterface;
    // 带宽管理器
    private BandwidthManager mBandwidthManager;

    public CallManager(Context context) {
        mContext = context;
        mBandwidthManager = new BandwidthManager();
    }

    public void destroy() {
        mBandwidthManager.destroy();
    }

    /** 挂断通话 */
    public void terminateCurrentCallOrConferenceOrAll() {
        Core core = LinphoneManager.getCore();
        Call call = core.getCurrentCall(); // 获取当前通话对象
        if (call != null) {
            call.terminate();
        } else if (core.isInConference()) { // 是否在会议中
            core.terminateConference(); // 挂断会议
        } else {
            core.terminateAllCalls(); // 挂断通话
        }
    }

    /** 添加一个视频通话 */
    public void addVideo() {
        Call call = LinphoneManager.getCore().getCurrentCall();
        if (call.getState() == Call.State.End || call.getState() == Call.State.Released) return;
        if (!call.getCurrentParams().videoEnabled()) { // 是否同意视频
            enableCamera(call, true); // 打开箱机
            reinviteWithVideo(); // 再次刷新视频状态
        }
    }

    /** 移除视频 */
    public void removeVideo() {
        Core core = LinphoneManager.getCore();
        Call call = core.getCurrentCall();
        CallParams params = core.createCallParams(call);
        params.enableVideo(false);
        call.update(params);
    }

    /** 切换摄像头 */
    public void switchCamera() {
        Core core = LinphoneManager.getCore();
        if (core == null) return;
        // 获取当前设备
        String currentDevice = core.getVideoDevice();
        Log.i("[Call Manager] Current camera device is " + currentDevice);
        // 获取视频设备列表
        String[] devices = core.getVideoDevicesList();
        for (String d : devices) {
            if (!d.equals(currentDevice) && !d.equals("StaticImage: Static picture")) {
                Log.i("[Call Manager] New camera device will be " + d);
                core.setVideoDevice(d); // 设置设备
                break;
            }
        }
        Call call = core.getCurrentCall();
        if (call == null) {
            Log.i("[Call Manager] Switching camera while not in call");
            return;
        }
        call.update(null);
    }

    /**
     * 接收呼叫
     *
     * @param call
     * @return
     */
    public boolean acceptCall(Call call) {
        if (call == null) return false;

        Core core = LinphoneManager.getCore();
        // 创建通话参数
        CallParams params = core.createCallParams(call);
        // 判断带宽条件
        boolean isLowBandwidthConnection =
                !LinphoneUtils.isHighBandwidthConnection(
                        LinphoneContext.instance().getApplicationContext());
        // 设置通话参数条件
        if (params != null) {
            // 设置低带宽
            params.enableLowBandwidth(isLowBandwidthConnection);
            // 设置响铃文件
            params.setRecordFile(
                    FileUtils.getCallRecordingFilename(mContext, call.getRemoteAddress()));
        } else {
            Log.e("[Call Manager] Could not create call params for call");
            return false;
        }
        // 接收通话
        call.acceptWithParams(params);
        return true;
    }

    /**
     * 接收通话更新
     *
     * @param accept
     */
    public void acceptCallUpdate(boolean accept) {
        Core core = LinphoneManager.getCore();
        Call call = core.getCurrentCall();
        if (call == null) {
            return;
        }
        CallParams params = core.createCallParams(call);
        if (accept) {
            params.enableVideo(true);
            // 是否允许截屏
            core.enableVideoCapture(true);
            // 是否允许视频显示
            core.enableVideoDisplay(true);
        }
        // 更新参数
        call.acceptUpdate(params);
    }

    /**
     * 邀请人ip地址
     *
     * @param address
     * @param forceZRTP 强制rtp
     */
    public void inviteAddress(Address address, boolean forceZRTP) {
        boolean isLowBandwidthConnection =
                !LinphoneUtils.isHighBandwidthConnection(
                        LinphoneContext.instance().getApplicationContext());

        inviteAddress(address, false, isLowBandwidthConnection, forceZRTP);
    }

    /**
     * 方法重载 是否启用低带宽
     *
     * @param address
     * @param videoEnabled
     * @param lowBandwidth
     */
    public void inviteAddress(Address address, boolean videoEnabled, boolean lowBandwidth) {
        inviteAddress(address, videoEnabled, lowBandwidth, false);
    }

    /**
     * 创建新的呼出 通过address呼出
     *
     * @param address
     */
    public void newOutgoingCall(AddressType address) {
        String to = address.getText().toString();
        newOutgoingCall(to, address.getDisplayedName());
    }

    /**
     * 通过to呼出
     *
     * @param to
     * @param displayName
     */
    public void newOutgoingCall(String to, String displayName) {
        if (to == null) return;

        // If to is only a username, try to find the contact to get an alias if existing
        if (!to.startsWith("sip:") || !to.contains("@")) {
            // 通过电话号码找到联系人
            LinphoneContact contact = ContactsManager.getInstance().findContactFromPhoneNumber(to);
            if (contact != null) {
                // 找到别名
                String alias = contact.getContactFromPresenceModelForUriOrTel(to);
                if (alias != null) {
                    to = alias;
                }
            }
        }

        LinphonePreferences preferences = LinphonePreferences.instance();
        Core core = LinphoneManager.getCore();
        Address address;
        address = core.interpretUrl(to); // InterpretUrl does normalizePhoneNumber
        if (address == null) {
            Log.e("[Call Manager] Couldn't convert to String to Address : " + to);
            return;
        }
        // 获取代理配置
        ProxyConfig lpc = core.getDefaultProxyConfig();
        // 获取
        if (mContext.getResources().getBoolean(R.bool.forbid_self_call)
                && lpc != null
                && address.weakEqual(lpc.getIdentityAddress())) {
            return;
        }
        address.setDisplayName(displayName);
        // 是否在低带宽条件下连接
        boolean isLowBandwidthConnection =
                !LinphoneUtils.isHighBandwidthConnection(
                        LinphoneContext.instance().getApplicationContext());

        if (core.isNetworkReachable()) {
            if (Version.isVideoCapable()) {
                boolean prefVideoEnable = preferences.isVideoEnabled(); // 是否允许视频通话
                boolean prefInitiateWithVideo = preferences.shouldInitiateVideoCall(); // 是否应该发起视频通话
                inviteAddress(
                        address,
                        prefVideoEnable && prefInitiateWithVideo,
                        isLowBandwidthConnection);
            } else {
                inviteAddress(address, false, isLowBandwidthConnection);
            }
        } else {
            Toast.makeText(
                            mContext,
                            mContext.getString(R.string.error_network_unreachable),
                            Toast.LENGTH_LONG)
                    .show();
            Log.e(
                    "[Call Manager] Error: "
                            + mContext.getString(R.string.error_network_unreachable));
        }
    }

    public void playDtmf(ContentResolver r, char dtmf) {
        try {
            if (Settings.System.getInt(r, Settings.System.DTMF_TONE_WHEN_DIALING) == 0) {
                // audible touch disabled: don't play on speaker, only send in outgoing stream
                return;
            }
        } catch (Settings.SettingNotFoundException e) {
            Log.e("[Call Manager] playDtmf exception: " + e);
        }

        LinphoneManager.getCore().playDtmf(dtmf, -1);
    }

    /**
     * 显示通话质量更新对话框
     *
     * @param call
     * @return
     */
    public boolean shouldShowAcceptCallUpdateDialog(Call call) {
        if (call == null) return true;

        boolean remoteVideo = call.getRemoteParams().videoEnabled();
        boolean localVideo = call.getCurrentParams().videoEnabled();
        boolean autoAcceptCameraPolicy =
                LinphonePreferences.instance().shouldAutomaticallyAcceptVideoRequests();
        return remoteVideo
                && !localVideo
                && !autoAcceptCameraPolicy
                && !call.getCore().isInConference();
    }

    /**
     * 设置call接口
     *
     * @param callInterface
     */
    public void setCallInterface(CallActivityInterface callInterface) {
        mCallInterface = callInterface;
    }

    /** 重置呼叫控制隐藏计时器 */
    public void resetCallControlsHidingTimer() {
        if (mCallInterface != null) {
            mCallInterface.resetCallControlsHidingTimer();
        }
    }
    /** 刷新通话行为 */
    public void refreshInCallActions() {
        if (mCallInterface != null) {
            mCallInterface.refreshInCallActions();
        }
    }

    /**
     * 从会议中结束
     *
     * @param call
     */
    public void removeCallFromConference(Call call) {
        if (call == null || call.getConference() == null) {
            return;
        }
        call.getConference().removeParticipant(call.getRemoteAddress());

        if (call.getCore().getConferenceSize() <= 1) {
            call.getCore().leaveConference();
        }
    }

    /** 暂停会议 */
    public void pauseConference() {
        Core core = LinphoneManager.getCore();
        if (core == null) return;
        if (core.isInConference()) {
            Log.i("[Call Manager] Pausing conference");
            core.leaveConference();
        } else {
            Log.w("[Call Manager] Core isn't in a conference, can't pause it");
        }
    }

    /** 重启会议 */
    public void resumeConference() {
        Core core = LinphoneManager.getCore();
        if (core == null) return;
        if (!core.isInConference()) {
            Log.i("[Call Manager] Resuming conference");
            core.enterConference();
        } else {
            Log.w("[Call Manager] Core is already in a conference, can't resume it");
        }
    }

    /**
     * 通过地址拨打
     *
     * @param address
     * @param videoEnabled
     * @param lowBandwidth
     * @param forceZRTP
     */
    private void inviteAddress(
            Address address, boolean videoEnabled, boolean lowBandwidth, boolean forceZRTP) {
        Core core = LinphoneManager.getCore();
        // 创建参数
        CallParams params = core.createCallParams(null);
        // 更新带宽参数
        mBandwidthManager.updateWithProfileSettings(params);

        if (videoEnabled && params.videoEnabled()) {
            params.enableVideo(true);
        } else {
            params.enableVideo(false);
        }
        // 设置低带宽下通话
        if (lowBandwidth) {
            params.enableLowBandwidth(true);
            Log.d("[Call Manager] Low bandwidth enabled in call params");
        }

        if (forceZRTP) {
            params.setMediaEncryption(MediaEncryption.ZRTP);
        }
        // 设置录音文件
        String recordFile =
                FileUtils.getCallRecordingFilename(
                        LinphoneContext.instance().getApplicationContext(), address);
        params.setRecordFile(recordFile);
        // 设置通话邀请参数
        core.inviteAddressWithParams(address, params);
    }

    /**
     * 二次邀请视频
     *
     * @return
     */
    private boolean reinviteWithVideo() {
        Core core = LinphoneManager.getCore();
        Call call = core.getCurrentCall();
        if (call == null) {
            Log.e("[Call Manager] Trying to add video while not in call");
            return false;
        }
        if (call.getRemoteParams().lowBandwidthEnabled()) { // 无法满足视频通话的条件
            Log.e("[Call Manager] Remote has low bandwidth, won't be able to do video");
            return false;
        }
        // 创建视频通话参数
        CallParams params = core.createCallParams(call);
        if (params.videoEnabled()) return false;

        // Check if video possible regarding bandwidth limitations 更新通话参数
        mBandwidthManager.updateWithProfileSettings(params);

        // Abort if not enough bandwidth... 如果没有足够的带宽，就终止…
        if (!params.videoEnabled()) {
            return false;
        }

        // Not yet in video call: try to re-invite with video 尝试用视频重新邀请
        call.update(params);
        return true;
    }

    /**
     * 是否允许摄像头
     *
     * @param call
     * @param enable
     */
    private void enableCamera(Call call, boolean enable) {
        if (call != null) {
            call.enableCamera(enable); // 设置摄像头参数
            // 设置是否显示来电通知
            if (mContext.getResources().getBoolean(R.bool.enable_call_notification))
                LinphoneContext.instance()
                        .getNotificationManager()
                        .displayCallNotification(LinphoneManager.getCore().getCurrentCall());
        }
    }
}
